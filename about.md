---
layout: page
title: Acerca de
permalink: /about/
---

Yo soy **La Razón** y **La Voz** y este pequeño y humilde espacio es **La Razón de La voz**
espero de verdad que este lugarcito sea de tu agrado y poder entretenerte.

Si te interesa estar al tanto de todo lo que hago, acá abajo estan las redes en las que
puedes **contactarme**:


+ Twitter: <https://twitter.com/razon_voz>
+ Correo: <larazondelavoz@disroot.org>
+ Web: <https://larazondelavoz.gitlab.io/>
+ Telegram: <https://t.me/larazondelavoz>
+ Feed Podcast: <https://larazondelavoz.gitlab.io/feed>
+ Mastodon: <https://mastodon.social/@larazondelavoz1>